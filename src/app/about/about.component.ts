import { AppComponent } from './../app.component';
import { Component, OnInit } from '@angular/core';
import { fadeInAnimation } from './../animations/fade-in-animation';


@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.css'],
  animations: [fadeInAnimation]
})
export class AboutComponent implements OnInit {

  VERSION: string;

  constructor() {
    this.VERSION = AppComponent.VERSION;
   }

  ngOnInit() {
  }

}
