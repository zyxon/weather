import { CityService } from './../city.service';
import { City } from './../entities/city';
import { Component, OnInit } from '@angular/core';
import { MdSnackBar } from '@angular/material';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, FormControl, Validators, Validator } from '@angular/forms';
import { fadeInAnimation } from './../animations/fade-in-animation';


@Component({
  selector: 'app-new-city',
  templateUrl: './new-city.component.html',
  styleUrls: ['./new-city.component.css'],
  animations: [fadeInAnimation]
})
export class NewCityComponent implements OnInit {

  cityTBA: City;
  addForm: FormGroup;

  constructor(
    private cityService: CityService,
    private snackBar: MdSnackBar,
    private fb: FormBuilder,
    private _router: Router
  ) {
    this.cityTBA = new City("", "");
  }

  ngOnInit() {
    this.cityTBA = new City("", "");

    this.addForm = this.fb.group({
      "name-in": new FormControl(this.cityTBA.name, [
        Validators.required,
        Validators.minLength(3)
      ]),
      "cc-in": new FormControl(this.cityTBA.countryCode, [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(2),
      ])
    });
  }

  dbg() {
    console.log(this.cityTBA);
  }

  submitNewCity(): void {
    /*this.cityService.addCity(this.cityTBA).then(() => {
      this.cityService.cityAdded.emit();
      this.snackBar.open(this.cityTBA.name + " added!", "Okay")
      this._router.navigate([
        "/weather-city/" + this.cityTBA.name
      ]);
    });*/

    this.cityService.addCity(this.cityTBA).then((status) => {
      if (status) {
        // sikeres
        this.snackBar.open(this.cityTBA.name + " added!", "Okay");
        this._router.navigate([
          "/weather-city",
          this.cityTBA.name
        ]);
      } else {
        // sikertelen
        this.snackBar.open("You are already tracking " + this.cityTBA.name + "!", "Okay");
      }
    });
  }



}
