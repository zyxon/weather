import { WeatherHelper } from './entities/weather-helper';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kmToMi'
})
export class KmToMiPipe implements PipeTransform {

  transform(value: number): number {
    return parseFloat(WeatherHelper.kmToMi(value).toFixed(1));
  }

}
