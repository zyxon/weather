import { WeatherHelper } from './weather-helper';


export class CurrentWeather {
  
  coord: {
    lon: number,
    lat: number
  };

  weather: [
    {
      id: number,
      icon: string,
      description: string,
      main: string
    }
  ];

  base: string;

  main: {
    temp: number,
    pressure: number,
    humidity: number,
    temp_min: number,
    temp_max: number
  };

  wind: {
    deg: number,
    speed: number
  };

  clouds: {
    all: number
  };

  dt: number;

  id: number;

  name: string;

  cod: number;

  sys: {
    type: number,
    id: number,
    message: number,
    sunrise: number,
    sunset: number
    country: string
  };

  visibility: number;

  ping(): void {
    console.log("This is a weather object");
  }
}