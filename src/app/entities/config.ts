export enum Measurement {
  METRIC=1,
  IMPERIAL=2
}

export class Config {
  private _measurement: Measurement;

  constructor(measurement: Measurement) {
    this._measurement = measurement;
  }

  get measurement(): Measurement {
    return this._measurement;
  }

  get measurementStr(): string {
    return Measurement[this.measurement];
  }

  get tempUnit(): string {
    switch(this._measurement) {
      case(Measurement.METRIC.valueOf()):
        return "°C";
      case (Measurement.IMPERIAL.valueOf()):
        return "°F";
    }
  }

  get pressureUnit(): string {
    switch(this._measurement) {
      case(Measurement.METRIC.valueOf()):
        return "hPa";
      case (Measurement.IMPERIAL.valueOf()):
        return "psi";
    }
  }

  get speedUnit(): string {
    switch(this._measurement) {
      case(Measurement.METRIC.valueOf()):
        return "km/h";
      case (Measurement.IMPERIAL.valueOf()):
        return "mph";
    }
  }

  get distanceUnit(): string {
    switch(this._measurement) {
      case(Measurement.METRIC.valueOf()):
        return "km";
      case (Measurement.IMPERIAL.valueOf()):
        return "mi";
    }
  }

}