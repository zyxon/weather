export class City {
  _name: string;
  _countryCode: string;

  constructor(name: string, countryCode: string) {
    this._name = name;
    this._countryCode = countryCode;
  }

  get name(){
    return this._name
  }

  set name(name: string) {
    this._name = name;
  }

  get countryCode() {
    return this._countryCode;
  }

  set countryCode(countryCode: string) {
    this._countryCode = countryCode;
  }

  getPrettyName(append: string): string {
    var str = this.name;
    if (append && append !== "") {
      str += append;
    }

    return str;
  }

  get nameAndCcode(): string {
    return this.name + ", " + this.countryCode.toUpperCase();
  }

  getUglyName() : string {
    return this.name + "," + this.countryCode;
  }
}