import { WeatherHelper } from './entities/weather-helper';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mToKm'
})
export class MToKmPipe implements PipeTransform {

  transform(value: number): number {
    return parseFloat(WeatherHelper.mToKm(value).toFixed(1));
  }

}
