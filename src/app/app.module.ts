import { ConfigService } from './config.service';
import { ForecastService } from './forecast.service';
import { CityService } from './city.service';
import { WeatherService } from './weather.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Routes, Router, RouterModule } from '@angular/router';


import { AppComponent } from './app.component';
import { WeatherCityComponent, ConfirmDeleteDialogComponent } from './weather-city/weather-city.component';
import { CityComponent } from './city/city.component';

import {
  MdButtonModule,
  MdToolbarModule,
  MdIconModule,
  MdSidenavModule,
  MdListModule,
  MdProgressSpinnerModule,
  MdCardModule,
  MdInputModule,
  MdGridListModule,
  MdSnackBarModule,
  MdDialogModule
} from '@angular/material';

import { KToCPipe } from './k-to-c.pipe';
import { NewCityComponent } from './new-city/new-city.component';
import { ForecastComponent } from './forecast/forecast.component';
import { ShortDatetimePipe } from './short-datetime.pipe';
import { CtofPipe } from './ctof.pipe';
import { HpaToPsiPipe } from './hpa-to-psi.pipe';
import { MpsToKphPipe } from './mps-to-kph.pipe';
import { KphToMphPipe } from './kph-to-mph.pipe';
import { MToKmPipe } from './m-to-km.pipe';
import { KmToMiPipe } from './km-to-mi.pipe';
import { AboutComponent } from './about/about.component';

const routes: Routes = [
  {
    path: 'weather-city/:name',
    component: WeatherCityComponent
  },
  {
    path: 'new-city',
    component: NewCityComponent
  },
  {
    path: 'forecast/:name',
    component: ForecastComponent
  },
  {
    path: 'about',
    component: AboutComponent
  }
];

@NgModule({
  declarations: [
    AppComponent,
    WeatherCityComponent,
    CityComponent,
    KToCPipe,
    NewCityComponent,
    ConfirmDeleteDialogComponent,
    ForecastComponent,
    ShortDatetimePipe,
    CtofPipe,
    HpaToPsiPipe,
    MpsToKphPipe,
    KphToMphPipe,
    MToKmPipe,
    KmToMiPipe,
    AboutComponent
  ],

  entryComponents: [ConfirmDeleteDialogComponent],

  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),
    ReactiveFormsModule,

    // Angular Material only beyond this point

    MdButtonModule,
    MdToolbarModule,
    MdIconModule,
    MdSidenavModule,
    MdListModule,
    MdProgressSpinnerModule,
    MdCardModule,
    MdInputModule,
    MdGridListModule,
    MdSnackBarModule,
    MdDialogModule
  ],
  providers: [WeatherService, CityService, ForecastService, ConfigService],
  bootstrap: [AppComponent],
})
export class AppModule { }
