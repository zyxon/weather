import { WeatherHelper } from './entities/weather-helper';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'cToF'
})
export class CtofPipe implements PipeTransform {

  transform(value: number): number {
    return parseFloat(WeatherHelper.CtoF(value).toFixed(1));
  }

}
