import { WeatherHelper } from './entities/weather-helper';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kToC'
})
export class KToCPipe implements PipeTransform {

  transform(value: number): number {
    return parseFloat(WeatherHelper.KtoC(value).toFixed(1));
  }

}
