import { WeatherHelper } from './entities/weather-helper';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'mpsToKph'
})
export class MpsToKphPipe implements PipeTransform {

  transform(value: number): number {
    return parseFloat(WeatherHelper.mpsToKph(value).toFixed(1));
  }

}
