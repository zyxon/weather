import { City } from './entities/city';
import { CurrentWeather } from './entities/current-weather';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';

declare var $: any;

@Injectable()
export class WeatherService {

  constructor(private http: Http) { }

  private static GET_CITY_WEATHER_URL: string = "http://api.openweathermap.org/data/2.5/weather?q=";
  private static API_KEY: string = "3cec5e36e276cf2346302ad49156e258";

  getWeatherForCity(city: City): Promise<CurrentWeather> {
    console.log("getting weather info for " + city.name);

    let queryString: string =
      WeatherService.GET_CITY_WEATHER_URL +
      city.getUglyName() +
      "&appid=" + WeatherService.API_KEY;

    return this.http.get(queryString)
      .toPromise().then(
      (response) => {
        return response.json() as CurrentWeather;
      }).catch((reason) => {
        return null;
      });
  }

}
