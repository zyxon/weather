import { fadeInAnimation } from './../animations/fade-in-animation';
import { Config, Measurement } from './../entities/config';
import { ConfigService } from './../config.service';
import { WeatherHelper } from './../entities/weather-helper';
import { CurrentWeather } from './../entities/current-weather';
import { Forecast } from './../entities/forecast';
import { ForecastService } from './../forecast.service';
import { CityService } from './../city.service';
import { City } from './../entities/city';
import { ActivatedRoute } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';

@Component({
  selector: 'app-forecast',
  templateUrl: './forecast.component.html',
  styleUrls: ['./forecast.component.css'],
  animations: [fadeInAnimation],
  host: {
    '[@fadeInAnimation]' : ''
  }
})
export class ForecastComponent implements OnInit {

  cf: Forecast = null;
  currentCity: City = null;
  isLoading: boolean = false;
  loadFail: boolean = false;
  config: Config;

  constructor(
    private _route: ActivatedRoute,
    private cityService: CityService,
    private configService: ConfigService,
    private forecastService: ForecastService,
    private loc: Location
  ) { }

  ngOnInit() {
    this.configService.getConfig().then((config) => {
      console.log("CONFIG");
      console.log(this.config);
      this.config = config;
    });

    this._route.params.subscribe(params => {
      this.isLoading = true;
      this.loadFail = false;
      this.cf = null;
      this.currentCity = null;

      if (params["name"]) {
        this.currentCity = this.cityService.getCity(params["name"]);
        if (this.currentCity) {
          // ok
          console.log("ok");
          this.forecastService.getForecastForCity(this.currentCity).then((forecast) => {
            console.log("GOT FORECAST FOR " + this.currentCity.name);
            console.log(forecast);

            this.handleForecastResponse(forecast);
          });

        } else {
          // null a currentcity
          console.log("nincs ilyen város")
          this.isLoading = false;
          this.loadFail = true;
        }
      } else {
        // nincs name param
        console.log("nincs név paraméter");
        this.isLoading = false;
        this.loadFail = true;
      }

    });
  }

  onSwipeRight(): void {
    this.loc.back();
  }

  retryGetForecast(): void {
    this.isLoading = true;
    this.loadFail = false;
    this.cf = null;

    if (this.currentCity) {
      this.forecastService.getForecastForCity(<City>this.currentCity).then((forecast) => {
        this.handleForecastResponse(forecast);
      });
    } else {
      console.log("retry nincs város");
      this.isLoading = false;
      this.loadFail = true;
    }
  }

  private handleForecastResponse(fc: Forecast) {
    if (fc) {
      this.cf = fc;
      this.isLoading = false;
      this.loadFail = false;

      console.log("wtf");
      console.log(this.cf);

      setTimeout(() => {
        for (var i = 0; i < this.cf.list.length; i++) {
          var li = this.cf.list[i];
          document.querySelector("#wicon-" + i)
            .classList.add(WeatherHelper.getWeatherIconClass(li.weather[0].id));
        }
      }, 10);


    } else {
      this.isLoading = false;
      this.loadFail = true;
    }
  }

}
