import { Config } from './../entities/config';
import { ConfigService } from './../config.service';
//import { fabLocations } from './../app.module';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { City } from './../entities/city';
import { CityService } from './../city.service';
import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { slideInOutAnimation } from './../animations/slide-in-out-animation';
import { fadeInAnimation } from './../animations/fade-in-animation';



import {
  trigger,
  state,
  style,
  animate,
  transition
} from '@angular/animations';

export const fabLocations: string[] = [
  "weather-city",
  "forecast",
  "about"
];

@Component({
  selector: 'city',
  templateUrl: './city.component.html',
  styleUrls: ['./city.component.css'],
  animations: [
    trigger('fabState', [
      state('inactive', style({
        transform: 'scale(0)'
      })),
      state('active', style({
        transform: 'scale(1)'
      })),
      transition('inactive => active', animate('300ms ease-in')),
      transition('active => inactive', animate('300ms ease-out'))
    ]),
    trigger('fabIconState', [
      state('inactive', style({
        transform: 'rotate(90deg)'
      })),
      state('active', style({
        transform: 'rotate(180deg)'
      })),
      transition('inactive => active', animate('400ms ease-in')),
      transition('active => inactive', animate('400ms ease-out'))
    ]),
    fadeInAnimation
  ]
})
export class CityComponent implements OnInit {

  _cities: City[];
  selectedCity: City;

  fabState: string = "active";

  constructor(
    private cityService: CityService,
    private _router: Router,
    private _route: ActivatedRoute
  ) { }

  getRouter() {
    return this._router;
  }

  ngOnInit() {
    this.cityService.getCities().then((cities) => {
      this._cities = cities;
    });

    this.cityService.cityAdded.subscribe(() => {
      this.cityService.getCities().then((cities) => {
        this._cities = cities;
        console.log("Cities reloaded. Cities:");
        console.log(this._cities);
      });
    });

    this._router.events.subscribe((val) => {
      if (val instanceof NavigationEnd) {
        this.isFabShown();
      }
    });
  }

  selectCity(city: City): void {
    this.selectedCity = city;
    this._router.navigate([
      "/weather-city",
      this.selectedCity.name
    ]);

  }

  private isFabShown(): boolean {

    if (this._router.url === "/") {
      this.fabState = "active";
      return true;
    } else {
      for (let s of fabLocations) {
        if (this._router.url.indexOf(s) > -1) {
          this.fabState = "active";
          return true;
        }
      }
      this.fabState = "inactive";
      return false;
    }
  }

}
