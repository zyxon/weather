import { Forecast } from './entities/forecast';
import { City } from './entities/city';
import { CurrentWeather } from './entities/current-weather';
import { Injectable } from '@angular/core';
import { Headers, Http } from '@angular/http';

import 'rxjs/add/operator/toPromise';


@Injectable()
export class ForecastService {

  private static GET_CITY_FORECAST_URL: string = "http://api.openweathermap.org/data/2.5/forecast?q=";
  private static API_KEY: string = "3cec5e36e276cf2346302ad49156e258";

  constructor(
    private http: Http
  ) { }

  getForecastForCity(city: City): Promise<Forecast> {
    let queryString: string =
    ForecastService.GET_CITY_FORECAST_URL +
    city.getUglyName() +
    "&appid=" + ForecastService.API_KEY;

    return this.http.get(queryString).toPromise().then((response) => {
      return response.json() as Forecast;
    }).catch((reason) => {
      return null;
    })
  }
}
