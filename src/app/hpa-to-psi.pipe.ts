import { WeatherHelper } from './entities/weather-helper';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'hpaToPsi'
})
export class HpaToPsiPipe implements PipeTransform {

  transform(value: number): number {
    return parseFloat(WeatherHelper.hPaToPsi(value).toFixed(1));
  }

}
