import { Config, Measurement } from './entities/config';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class ConfigService {

  constructor() { }

  config: Config = null;

  configChanged = new EventEmitter();

  getConfig(): Promise<Config> {
    return new Promise((resolve) => {
      if (!this.config) {
        try {
          this.config = new Config(JSON.parse(localStorage.WeatherConfig)._measurement);
        } catch (e) {
          console.log("new config");
          this.config = new Config(Measurement.METRIC);
        }
      }
      resolve(this.config);
    });
  }

  changeMeasurement(mea: Measurement) {
    this.config = new Config(mea);
    this.configChanged.emit();
    this.setConfig();
  }

  setConfig(): Promise<null> {
    return new Promise((resolve) => {
      localStorage.WeatherConfig = JSON.stringify(this.config);
      resolve(null);
    });
  }

}
