import { WeatherHelper } from './entities/weather-helper';
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'kphToMph'
})
export class KphToMphPipe implements PipeTransform {

  transform(value: number): number {
    return parseFloat(WeatherHelper.kphToMph(value).toFixed(1));
  }

}
