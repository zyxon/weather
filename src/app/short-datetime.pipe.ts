import { Pipe, PipeTransform } from '@angular/core';

const MONTHS = [
  "Jan",
  "Feb",
  "Mar",
  "Apr",
  "May",
  "Jun",
  "Jul",
  "Aug",
  "Sep",
  "Oct",
  "Nov",
  "Dec"
]

@Pipe({
  name: 'shortDatetime'
})
export class ShortDatetimePipe implements PipeTransform {

  transform(value: string): string {
    // in format: 2017-09-26 09:00:00

    try {
      var date = new Date(value + " UTC");

      var retStr =
        MONTHS[date.getMonth()] + " " +
        this.getNumeral(date.getDate()) + " " +
        date.getHours() + ":" +
        (date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes())

      return retStr;  // Dec 3rd 10:00
    } catch (e) {
      console.error("could not parse value: " + value);
      return value;
    }
  }

  getNumeral(inNum: number): string {
    try {

      switch (inNum) {
        case 1:
          return "1st";

        case 2:
          return "2nd";

        case 3:
          return "3rd";

        case 21:
          return "21st";

        case 22:
          return "22nd";

        case 23:
          return "23rd";

        case 31:
          return "31st";

        default:
          return inNum + "th";
      }
    } catch (e) {
      console.error("could not parse value: " + inNum);
      return inNum + "";
    }
  }

}


