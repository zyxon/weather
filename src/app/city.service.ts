import { by } from 'protractor';
import { City } from './entities/city';
import { Injectable, EventEmitter } from '@angular/core';

@Injectable()
export class CityService {

  selectedCityChanged = new EventEmitter();
  cityAdded = new EventEmitter();

  private CITIES: City[] = [];

  constructor() { }

  getCities(): Promise<City[]> {
    return new Promise(resolve => {
      var returnArr: City[] = [];
      try {
        returnArr = JSON.parse(localStorage.WeatherCities);
        for (var i = 0; i < returnArr.length; i++) {
          var newCity = new City(returnArr[i]._name, returnArr[i]._countryCode);
          returnArr[i] = newCity;
        }
      } catch (e) {
        returnArr = []
        localStorage.WeatherCities = JSON.stringify(returnArr);
      } finally {
        this.CITIES = returnArr;
        resolve(returnArr);
      }
    });
  }

  addCity(city: City): Promise<boolean> {
    return new Promise(resolve => {

      let doAdd: boolean = true;

      for (var strdCity of this.CITIES) {
        if (strdCity.name.toUpperCase() === city.name.toUpperCase()) {
          doAdd = false;
          resolve(false);
        }
      }

      if (doAdd) {
        this.CITIES.push(city);
        localStorage.WeatherCities = JSON.stringify(this.CITIES);
        this.cityAdded.emit();
        resolve(true);
      }

      
    });
  }

  getCity(name: string): City {
    for (let city of this.CITIES) {
      if (city.name.toUpperCase() === name.toUpperCase()) {
        return city;
      }
    }
    return null;
  }

  deleteCity(city: City): Promise<null> {
    return new Promise(resolve => {
      var idx: number = null;
      for (var i = 0; i < this.CITIES.length; i++) {
        let c = this.CITIES[i];
        if (c.name === city.name) {
          idx = i;
          break;
        }
      }

      if (idx !== null) {
        this.CITIES.splice(idx, 1);
        localStorage.WeatherCities = JSON.stringify(this.CITIES);
      }

      resolve(null);
    })
  }

}
