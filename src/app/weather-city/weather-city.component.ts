import { fadeInAnimation } from './../animations/fade-in-animation';
import { Config, Measurement } from './../entities/config';
import { ConfigService } from './../config.service';
import { WeatherHelper } from './../entities/weather-helper';
import { CurrentWeather } from './../entities/current-weather';
import { City } from './../entities/city';
import { CityService } from './../city.service';
import { CityComponent } from './../city/city.component';
import { WeatherService } from './../weather.service';
import { Component, OnInit, Injectable, Inject } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MdDialog, MdDialogRef, MD_DIALOG_DATA } from '@angular/material';


@Component({
  selector: 'weather-city',
  templateUrl: './weather-city.component.html',
  styleUrls: ['./weather-city.component.css'],
  providers: [CityComponent],
  animations: [fadeInAnimation]
})
export class WeatherCityComponent implements OnInit {

  isLoading: boolean = false;
  loadFail: boolean = false;
  cw: CurrentWeather = null;
  currentCity: City = null;
  config: Config = null;

  SWIPE_ACTION = { LEFT: 'swipeleft', RIGHT: 'swiperight' };


  constructor(
    private weatherService: WeatherService,
    private cityService: CityService,
    private configService: ConfigService,
    private _route: ActivatedRoute,
    private _router: Router,
    public deleteDialog: MdDialog
  ) {

    this.configService.getConfig().then((config) => {
      this.config = config;
    })

    this.configService.configChanged.subscribe(() => {
      this.configService.getConfig().then((config) => this.config = config);
    })

    this._route.params.subscribe(params => {
      this.isLoading = true;
      this.loadFail = false;
      this.cw = null;
      this.currentCity = null;

      if (params["name"]) {
        var city = this.cityService.getCity(params["name"]);
        this.currentCity = city;

        if (city) {
          console.log("CHANGE! " + city.name);

          weatherService.getWeatherForCity(<City>city).then((weather) => {
            this.handleWeatherServiceResponse(weather);
          });
        } else {
          console.log("nincs ilyen város");
          this.isLoading = false;
          this.loadFail = true;
        }
      } else {
        console.log("nincs város megadva");
        this.isLoading = false;
        this.loadFail = true;
      }
    })
  }

  ngOnInit() {

  }

  onSwipeLeft() {
    console.log("SWIPE LEFT");
    this._router.navigate([
      "/forecast",
      this.currentCity.name
    ]);
  }

  retryGetWeather(): void {
    this.isLoading = true;
    this.loadFail = false;
    this.cw = null;

    if (this.currentCity) {
      this.weatherService.getWeatherForCity(<City>this.currentCity).then((weather) => {
        this.handleWeatherServiceResponse(weather);
      });
    } else {
      console.log("retry nincs város");
      this.isLoading = false;
      this.loadFail = true;
    }
  }

  private handleWeatherServiceResponse(weather: CurrentWeather): void {
    console.log(weather);
    this.isLoading = false;
    if (weather) {  // success
      this.loadFail = false;
      this.cw = weather;

      console.log(WeatherHelper.getWeatherIconClass(weather.weather[0].id));
      console.log(WeatherHelper.getWindIconClass(weather.wind.deg));
      setTimeout(function () {
        try {
          document.querySelector("#current_weather_avatar")
            .classList.add(WeatherHelper.getWeatherIconClass(weather.weather[0].id));
        } catch (e) {
          console.log("error_current_weather");
        }

        try {
          document.querySelector("#wind-dir-icon")
            .classList.add(WeatherHelper.getWindIconClass(weather.wind.deg));
        } catch (e) {
          console.log("error_wind_deg");
        }

        
      }, 100);
    } else {        // error
      console.log("ERROR");
      this.loadFail = true;
    }
  }

  triggerDelete(): void {

    let dialogRef = this.deleteDialog.open(ConfirmDeleteDialogComponent, {
      data: {
        city: this.currentCity.name,
        action: null
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log("Dialog closed");
      console.log("Result: " + result);
      if (result) {
        this.cityService.deleteCity(this.currentCity).then(() => {
          this.cityService.cityAdded.emit();
          this._router.navigate(['/']);
        });
      }
    });

  }

  changeUnits(): void {
    if (this.config.measurementStr === "METRIC") {
      this.configService.changeMeasurement(Measurement.IMPERIAL);
    } else if (this.config.measurementStr === "IMPERIAL") {
      this.configService.changeMeasurement(Measurement.METRIC);
    }
  }

}

@Component({
  selector: "confirm-delete-dialog",
  template: `
  <div class="mat-typography">
    <h1 md-dialog-title>Really?</h1>
    <div md-dialog-content>
      <p class="body-1">Are you sure you want to delete {{data.city}}?</p>
    </div>
    <div md-dialog-actions>
      <md-grid-list cols="2">
        <button md-raised-button color="warn" [md-dialog-close]="true" tabindex="2">DELETE</button>
        <button md-button color="primary" (click)="onNoClick()" tabindex="-1">CANCEL</button>
      </md-grid-list>
    </div>
  </div>
  `
})
export class ConfirmDeleteDialogComponent {
  constructor(
    public dialogRef: MdDialogRef<ConfirmDeleteDialogComponent>,
    @Inject(MD_DIALOG_DATA) public data: any) {

  }

  onNoClick(): void {
    this.dialogRef.close();
  }
}